import React from "react"
import PropTypes from "prop-types"

import "./button.css"

const Button = ({ action, logo = "", text = "", disabled_initial_state = false }) => {
  const clickedClass = " clicked"
  const disabledClass = " disabled"

  const [clicked, setClicked] = React.useState(false)
  const [disabled, setDisabled] = React.useState(disabled_initial_state)

  const handleClick = event => {
    if (!disabled) {
      if (clicked) {
        setClicked(false)
      } else {
        setClicked(true)
        action()
      }
    }
  }

  return (
    <button
      className={`button${clicked ? clickedClass : ""}${disabled ? disabledClass : ""}`}
      onClick={handleClick}
    > 
    {logo != "" ? <img className={"logo"} src={logo}/> : null}
      <span className={"text"}>{text}</span>
    </button>
  )
}

Button.propTypes = {
    disabled: PropTypes.bool,
    logo: PropTypes.string,
    text: PropTypes.string,
    action: PropTypes.func,
  };

export default Button
