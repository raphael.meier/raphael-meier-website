import React from "react"

import "../styles/main.css"

import Button from "../components/Button/button"

import git from "../resources/images/git.svg"

export default function Home() {
  return (
    <div>
      <div>HelloWorld</div>
      <div className="network-links">
        <Button action={() => {console.log("Test")}} text={"Button"}></Button>
        <Button action={() => {console.log("Gitlab")}} logo={git}></Button>
        <Button action={() => {console.log("Text + logo")}} logo={git} text={"Gitlab"}></Button>
      </div>
    </div>
    
    
  )
}
